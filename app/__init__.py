from flask import Flask
from .extensions import mongo

from app.webhook.routes import webhook


# Creating our flask app
def create_app(config='app.settings'):

    app = Flask(__name__)

    # registering all the blueprints
    app.register_blueprint(webhook)
    app.config.from_object(config)

    try:
        mongo.init_app(app)
    except Exception as e:
        print(e)
    return app
