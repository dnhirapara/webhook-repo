from threading import Event
from flask import Blueprint, json, request, jsonify
from app.extensions import mongo
import time

webhook = Blueprint('Webhook', __name__, url_prefix='/webhook')

@webhook.route('/receiver', methods=["POST"])
def receiver():
    """
    PR -> "number", "sender.login", "type - action", "head.ref from-branch", "base.ref to-branch", created_at
    """
    if request.headers.get('Content-Type') == 'application/json':
        request_data = json.loads(json.dumps(request.json))
        with open('temp.json','w') as f:
            f.write(json.dumps(request_data))
        event_type = request.headers.get('X-GitHub-Event') # 'push' or 'pull_request'
        db = mongo.db
        if event_type == 'push':
            common_info = {
                "action":event_type,
                "from_branch":request_data.get('ref'),
                "to_branch":"",
            }
            commits = request_data.get('commits')
            for commit in commits:
                new_commit = {
                    "author":commit.get('author').get('username'),
                    "request_id":commit.get('id'),
                    "timestamp":commit.get('timestamp'),
                }
                new_commit.update(common_info)
                try:
                    db.webhook.insert_one(new_commit)
                except Exception as e:
                    print(new_commit)
                    print(e)
                    return {"Error Occured!!!"}, 500
        elif event_type == 'pull_request':
            pr = {
                "request_id":request_data.get('number'),
                "author":request_data.get('sender').get('login'),
                "action":event_type,
                "from_branch":request_data.get('pull_request').get('head').get('ref'),
                "to_branch":request_data.get('pull_request').get('base').get('ref'),
                "timestamp":request_data.get('pull_request').get('created_at')
            }
            try:
                db.webhook.insert_one(pr)
            except Exception as e:
                print(pr)
                print(e)
                return {"Error Occured!!!"}, 500
        else:
            return {"404 Not Found"}, 404
    return {}, 200

@webhook.route('/')
def index():
    db = mongo.db
    while True:
        data_source = db.webhook.find()
        data_dict = []
        for data in data_source:
            data_dict.append(data)
            return jsonify((data)